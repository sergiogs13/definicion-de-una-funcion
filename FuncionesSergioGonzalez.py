d = {
    'LHY': 'Roberto',
    'HRT': 'Juan',
    'EEr': 'Maria',
}

print(d)


def print_message(name):
    print(f'Hello world! My name is {name}')


def sum_list(start: int, end: int) -> int:
    new_list = list(range(start, end + 1))
    s = sum(new_list)
    return s


a = sum_list(1, 3)
print(a)


def sum_product(*args):
    x = args[0] + 2 * sum(args[1:])
    return x


from math import sin, cos


def trig(x: float) -> float:
    y = sin(x) ** 2 + cos(x) ** 2
    return y


print(trig(2))

print(sorted([2,3,2], reverse=true))



def f(student):
    return student[2]



def sum_product(my_list: list) -> int:
    x = my_list[0] + 2 * sum(my_list[1:])
    return x


print(sum_product([3, 3, 4, 5]))